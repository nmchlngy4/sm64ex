# sm64ex
Fork of [sm64-port/sm64-port](https://github.com/sm64-port/sm64-port) with additional features. 

## What is it?
This code repository contains source code required for building sm64ex, a enhanced version of the PC port of the hit 1996 3D Action video game Super Mario 64. It has been cloned from [sm64pc/sm64ex] (https://github.com/sm64pc/sm64ex) for preservation and also further development.

**Please note that a ROM file for Super Mario 64 is required and is not provided with the repository for legal reasons. You will have to find one on your own.**

## How can I help?

Feel free to report bugs and contribute, but remember, there must be **no upload of any copyrighted asset**. 
Run `./extract_assets.py --clean && make clean` or `make distclean` to remove any Nintendo assets. If you don't, then Nintendo is going to go after me.

Please contribute **first** to the [nightly branch](https://gitlab.com/nmchlngy4/sm64ex/-/tree/nightly). New functionality will be merged to master once they're considered to be well-tested.

# I don't speak English, can I read this in other languages?

*Sure, you can also read this file in the following languages: [Español](README_es_ES.md), [Português](README_pt_BR.md) or [简体中文](README_zh_CN.md).*

## So, what's diffrent between the N64 version / Super Mario 3D All-Stars and sm64ex?

 * Options menu with various settings, including the ability to remap buttons to your liking.
 * Ability to load external data (so far, only textures and assembled soundbanks are supported), which allows for custom texture packs.
 * Optional analog camera and mouse look (using [Puppycam](https://github.com/FazanaJ/puppycam)).
 * Optional OpenGL1.3-based renderer for older machines, as well as the original GL2.1, D3D11 and D3D12 renderers from Emill's [n64-fast3d-engine](https://github.com/Emill/n64-fast3d-engine/).
 * Option to disable drawing distances. Now you can see the entire stage!
 * Optional model and texture fixes (e.g. the smoke texture).
 * Tired of watching the same Peach and Lakitu cutscenes at the beginning of the game? Well, now you can skip it with the `--skip-intro` CLI option
 * Cheats menu in Options (activate with `--cheats` or by pressing L thrice in the pause menu).
 * Support for both little-endian and big-endian save files (meaning you can use save files from both sm64-port and most emulators), as well as an optional text-based save format.

Recent changes in Nightly have moved the save and configuration file path to `%HOMEPATH%\AppData\Roaming\sm64pc` on Windows and `$HOME/.local/share/sm64pc` on Linux. This behaviour can be changed with the `--savepath` CLI option.
For example `--savepath .` will read saves from the current directory (which not always matches the exe directory, but most of the time it does);
   `--savepath '!'` will read saves from the executable directory.

## How do I build it?
For building instructions, please refer to the [wiki](https://github.com/sm64pc/sm64ex/wiki).

**Make sure you have MXE first before attempting to compile for Windows on Linux and WSL. Follow the guide on the wiki.**

## Why did you clone it here?
It has become more apparent that GitHub is cracking down on its users, as well as breaking critical functionality by politicizing the names of code branches. I have decided to continue maintaining this project on GitLab as an alternative option to GitHub.

As time progresses, I will add build instructions, dependencies, and new translations of this readme to this repository.